<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionProducto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-producto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'producto_id')->textInput() ?>

    <?= $form->field($model, 'producto_tipo_id')->textInput() ?>

    <?= $form->field($model, 'cotizacion_id')->textInput() ?>

    <?= $form->field($model, 'cotizacion_RUC_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cotizacion_persona_cliente_id')->textInput() ?>

    <?= $form->field($model, 'cotizacion_RUC_vendedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cotizacion_persona_vendedor_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
