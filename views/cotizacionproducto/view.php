<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionProducto */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacion Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-producto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_cliente_id' => $model->cotizacion_persona_cliente_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor_id' => $model->cotizacion_persona_vendedor_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_cliente_id' => $model->cotizacion_persona_cliente_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor_id' => $model->cotizacion_persona_vendedor_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cantidad',
            'producto_id',
            'producto_tipo_id',
            'cotizacion_id',
            'cotizacion_RUC_cliente',
            'cotizacion_persona_cliente_id',
            'cotizacion_RUC_vendedor',
            'cotizacion_persona_vendedor_id',
        ],
    ]) ?>

</div>
