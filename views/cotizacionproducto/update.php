<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CotizacionProducto */

$this->title = 'Update Cotizacion Producto: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacion Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_cliente_id' => $model->cotizacion_persona_cliente_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor_id' => $model->cotizacion_persona_vendedor_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cotizacion-producto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
