<?php

/* @var $this yii\web\View */

$this->title = 'Cotizar';
?>
<div class="site-index">

    <div>
        <h1>Prueba Técnica</h1>
        <!-- <p class="lead">Valor Technology</p> -->
        <img class="logo-valor"  src="assets/valor.png" alt="">
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
                <p>
                    Se requiere desarrollar un módulo que permita agilizar el proceso de cotización de productos 
                    a un cliente de manera automatizada.<br>El equipo de trabajo desea tener la posibilidad de 
                    agregar productos a la cotización, formar paquetes de productos por promociones y aplicación 
                    de descuentos e impuestos.
                </p>
            </div>
            <div class="col-lg-12">
                <p class="requisitos" >Los campos necesarios para generar dicha cotización son: </p>
                <ul>
                    <li>Nombre del cliente</li>
                    <li>RUC</li>
                    <li>Nombre del vendedor</li>
                    <li>Tipo de producto</li>
                    <li>Monto</li>
                    <li>Impuestos</li>
                    <li>Descuentos</li>
                </ul>
            </div>
        </div>

    </div>
</div>
