<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Persona;
use app\models\Producto;
use app\models\Tipo;
use app\models\Paquetes;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Cotizacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cotizacion-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'RUC_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'RUC_vendedor')->textInput(['maxlength' => true]) ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><i class="glyphicon glyphicon-gift"></i>Productos</h4>
        </div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper',
                'widgetBody' => '.container-items',
                'widgetItem' => '.item',
                'limit' => 100,
                'min' => 1,
                'insertButton' => '.add-item',
                'deleteButton' => '.remove-item',
                'model' => $modelsCP[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'producto_id',
                    'cantidad',               
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsCP as $i => $CP): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Producto</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">                
                        <div class="row">
                            <div class="col-sm-4">
                                <?= $form->field($CP, "[{$i}]producto_id")->dropDownList(
                                    ArrayHelper::map(Producto::find()->all(),'id','nombre'),
                                    ['prompt'=>'Elegir Producto']) 
                                ?>  
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($CP, "[{$i}]cantidad")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>
</div>

<div class="panel panel-default">
        <div class="panel-heading">
            <h4><i class="glyphicon glyphicon-envelope"></i> Paquete</h4>
        </div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_package', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items-package', // required: css class selector
                'widgetItem' => '.item-package', // required: css class
                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item-package', // css class
                'deleteButton' => '.remove-item-package', // css class
                'model' => $modelsPC[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'paquetes_id',
                ],
            ]); ?>

            <div class="container-items-package"><!-- widgetContainer -->
            <?php foreach ($modelsPC as $i => $PC): ?>
                <div class="item-package panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Paquete</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item-package btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item-package btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?= $form->field($PC, "[{$i}]paquetes_id")->dropDownList(
                            ArrayHelper::map(Paquetes::find()->all(),'id','nombre'),
                            ['prompt'=>'Elegir Paquete']) 

                        ?>
                      
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
