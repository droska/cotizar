<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Persona;
use app\models\Oferta;
use app\models\CotizacionProducto;

/* @var $this yii\web\View */
/* @var $model app\models\Cotizacion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cotizacion-view">

    <h4>Cotización</h4>
    <h4>
        <?php 
            $now = new DateTime();
            echo $now->format('d / F / Y');
        ?>
    </h4>
    <br>

    <?php //Reemplazar <?php por <?=
        /*<?= Html::encode($this->title) ?>
            DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'RUC_cliente',
                'RUC_vendedor',
            ],
        ])*/
    ?>

    <div class="row">    
        <div class="col-sm-3 bf">
            <p>
                <?php $cliente = Persona::findOne($model->RUC_cliente); 
                    echo "Cliente: ".$cliente->nombre."<br>RUC: ".$cliente->RUC;
                ?>
            <p>
            <p>
                <?php $vendedor = Persona::findOne($model->RUC_vendedor); 
                    echo "Vendedor: ".$vendedor->nombre;
                ?>
            <p>
        </div>
    </div>

    <h4>Productos</h4>
    <?php
        $aprod = 0; 
        foreach ($model->cotizacionProductos as $productos) {
    ?>
    <!-- for de productos -->
            <div class="row">
                <div class="col-sm-3 bf">
                    <?php 
                        $cant = $productos->cantidad;
                        echo $productos->producto->nombre."<br>Precio: ".$productos->producto->monto."<br>Cantidad: "
                        .$cant;
                        $aprod += $productos->producto->monto*$cant;
                    ?>
                </div>
            </div>
    <?php 
        }        
    ?>    

    <h4>Paquetes</h4>
    <?php 
        $apaq = 0; 
        foreach ($model->detalleCotizacions as $value) {
    ?>
    <!-- for de productos -->
            <div class="row">
                <div class="col-sm-3 bf">
                    <?php  
                        $preciopaq = Oferta::findOne($value->paquetes_id);
                        $desc = Oferta::findOne($value->paquetes_id);
                        echo "Paquete: ".$value->paquetes->nombre."<br> Precio: ".$preciopaq->monto."<br>
                        Descuento: ".$desc->descuento."%";

                        $sumapaquete = $preciopaq->monto - ($preciopaq->monto * $desc->descuento)/100;
                    ?>
                </div>
            </div>
    <?php 
         $apaq += $sumapaquete;
        }        
    ?>
    <br>
    <div class="row">
        <div class="col-sm-3">
        
            <?php 
                $imp = 0.12;
                $subt = $apaq + $aprod; 
            ?> 
            <?= "Subtotal: ".$subt."<br>"."Impuesto: ".$imp*(100)."%"?>
            <h3>
                Total:
                <?= " ".($subt+($subt*$imp)); ?>
            </h3>

        </div>
    </div>

</div>
