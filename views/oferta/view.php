<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Oferta */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ofertas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oferta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'paquetes_id' => $model->paquetes_id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'paquetes_id' => $model->paquetes_id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cantidad_productos',
            'descuento',
            'monto',
            'paquetes_id',
            'producto_id',
            'producto_tipo_id',
        ],
    ]) ?>

</div>
