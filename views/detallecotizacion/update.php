<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleCotizacion */

$this->title = 'Update Detalle Cotizacion: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Detalle Cotizacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_rol_id' => $model->cotizacion_persona_rol_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor' => $model->cotizacion_persona_vendedor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="detalle-cotizacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
