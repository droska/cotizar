<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleCotizacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-cotizacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'subtotal') ?>

    <?= $form->field($model, 'impuesto') ?>

    <?= $form->field($model, 'total') ?>

    <?= $form->field($model, 'cotizacion_id') ?>

    <?php // echo $form->field($model, 'cotizacion_RUC_cliente') ?>

    <?php // echo $form->field($model, 'cotizacion_persona_rol_id') ?>

    <?php // echo $form->field($model, 'cotizacion_RUC_vendedor') ?>

    <?php // echo $form->field($model, 'cotizacion_persona_vendedor') ?>

    <?php // echo $form->field($model, 'paquetes_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
