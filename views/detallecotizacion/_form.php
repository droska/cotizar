<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleCotizacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-cotizacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subtotal')->textInput() ?>

    <?= $form->field($model, 'impuesto')->textInput() ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'cotizacion_id')->textInput() ?>

    <?= $form->field($model, 'cotizacion_RUC_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cotizacion_persona_rol_id')->textInput() ?>

    <?= $form->field($model, 'cotizacion_RUC_vendedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cotizacion_persona_vendedor')->textInput() ?>

    <?= $form->field($model, 'paquetes_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
