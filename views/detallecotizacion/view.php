<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleCotizacion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Detalle Cotizacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-cotizacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_rol_id' => $model->cotizacion_persona_rol_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor' => $model->cotizacion_persona_vendedor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_rol_id' => $model->cotizacion_persona_rol_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor' => $model->cotizacion_persona_vendedor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'subtotal',
            'impuesto',
            'total',
            'cotizacion_id',
            'cotizacion_RUC_cliente',
            'cotizacion_persona_rol_id',
            'cotizacion_RUC_vendedor',
            'cotizacion_persona_vendedor',
            'paquetes_id',
        ],
    ]) ?>

</div>
