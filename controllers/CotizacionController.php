<?php

namespace app\controllers;

use Yii;
use app\models\Cotizacion;
use app\models\CotizacionSearch;
use app\models\CotizacionProducto;
use app\models\DetalleCotizacion;
use app\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CotizacionController implements the CRUD actions for Cotizacion model.
 */
class CotizacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cotizacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CotizacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cotizacion model.
     * @param integer $id
     * @param string $RUC_cliente
     * @param string $RUC_vendedor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $RUC_cliente, $RUC_vendedor)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $RUC_cliente, $RUC_vendedor),
        ]);
    }

    /**
     * Creates a new Cotizacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cotizacion();
        $modelsCP = [new CotizacionProducto];
        $modelsPC = [new DetalleCotizacion];

        if ($model->load(Yii::$app->request->post())) {

            $modelsCP = Model::createMultiple(CotizacionProducto::classname());
            Model::loadMultiple($modelsCP, Yii::$app->request->post());

            $modelsPC = Model::createMultiple(DetalleCotizacion::classname());
            Model::loadMultiple($modelsPC, Yii::$app->request->post());

            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {    

                    foreach ($modelsCP as $objCP) {
                        $objCP->cotizacion_id = $model->id;
                        if (! ($flag = $objCP->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }
                    foreach ( $modelsPC as $objPC) {
                        $objPC->cotizacion_id = $model->id;
                        if (! ($flag = $objPC->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }
                }
                if ($flag) {
                    $model->save(false);
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id, 'RUC_cliente' => $model->RUC_cliente, 'RUC_vendedor' => $model->RUC_vendedor]);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }          
        }else{

            return $this->render('create', [
                'model' => $model,
                'modelsCP' => (empty($modelsPC)) ? [new CotizacionProducto] : $modelsCP,
                'modelsPC' => (empty($modelsPC)) ? [new DetalleCotizacion] : $modelsPC
            ]);
        }

    }

    /**
     * Updates an existing Cotizacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $RUC_cliente
     * @param string $RUC_vendedor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $RUC_cliente, $RUC_vendedor)
    {
        $model = $this->findModel($id, $RUC_cliente, $RUC_vendedor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'RUC_cliente' => $model->RUC_cliente, 'RUC_vendedor' => $model->RUC_vendedor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cotizacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $RUC_cliente
     * @param string $RUC_vendedor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $RUC_cliente, $RUC_vendedor)
    {
        $this->findModel($id, $RUC_cliente, $RUC_vendedor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cotizacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $RUC_cliente
     * @param string $RUC_vendedor
     * @return Cotizacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $RUC_cliente, $RUC_vendedor)
    {
        if (($model = Cotizacion::findOne(['id' => $id, 'RUC_cliente' => $RUC_cliente, 'RUC_vendedor' => $RUC_vendedor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
