<?php

namespace app\controllers;

use Yii;
use app\models\DetalleCotizacion;
use app\models\DetalleCotizacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DetalleCotizacionController implements the CRUD actions for DetalleCotizacion model.
 */
class DetalleCotizacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DetalleCotizacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DetalleCotizacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetalleCotizacion model.
     * @param integer $id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_rol_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_rol_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_rol_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor),
        ]);
    }

    /**
     * Creates a new DetalleCotizacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DetalleCotizacion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_rol_id' => $model->cotizacion_persona_rol_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor' => $model->cotizacion_persona_vendedor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DetalleCotizacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_rol_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_rol_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor)
    {
        $model = $this->findModel($id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_rol_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_rol_id' => $model->cotizacion_persona_rol_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor' => $model->cotizacion_persona_vendedor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DetalleCotizacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_rol_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_rol_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor)
    {
        $this->findModel($id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_rol_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetalleCotizacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_rol_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor
     * @return DetalleCotizacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_rol_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor)
    {
        if (($model = DetalleCotizacion::findOne(['id' => $id, 'cotizacion_id' => $cotizacion_id, 'cotizacion_RUC_cliente' => $cotizacion_RUC_cliente, 'cotizacion_persona_rol_id' => $cotizacion_persona_rol_id, 'cotizacion_RUC_vendedor' => $cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor' => $cotizacion_persona_vendedor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
