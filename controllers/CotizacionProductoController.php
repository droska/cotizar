<?php

namespace app\controllers;

use Yii;
use app\models\CotizacionProducto;
use app\models\CotizacionProductoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CotizacionProductoController implements the CRUD actions for CotizacionProducto model.
 */
class CotizacionProductoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CotizacionProducto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CotizacionProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CotizacionProducto model.
     * @param integer $id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_cliente_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $producto_id, $producto_tipo_id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_cliente_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $producto_id, $producto_tipo_id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_cliente_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor_id),
        ]);
    }

    /**
     * Creates a new CotizacionProducto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CotizacionProducto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_cliente_id' => $model->cotizacion_persona_cliente_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor_id' => $model->cotizacion_persona_vendedor_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CotizacionProducto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_cliente_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $producto_id, $producto_tipo_id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_cliente_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor_id)
    {
        $model = $this->findModel($id, $producto_id, $producto_tipo_id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_cliente_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id, 'cotizacion_id' => $model->cotizacion_id, 'cotizacion_RUC_cliente' => $model->cotizacion_RUC_cliente, 'cotizacion_persona_cliente_id' => $model->cotizacion_persona_cliente_id, 'cotizacion_RUC_vendedor' => $model->cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor_id' => $model->cotizacion_persona_vendedor_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CotizacionProducto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_cliente_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $producto_id, $producto_tipo_id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_cliente_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor_id)
    {
        $this->findModel($id, $producto_id, $producto_tipo_id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_cliente_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CotizacionProducto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @param integer $cotizacion_id
     * @param string $cotizacion_RUC_cliente
     * @param integer $cotizacion_persona_cliente_id
     * @param string $cotizacion_RUC_vendedor
     * @param integer $cotizacion_persona_vendedor_id
     * @return CotizacionProducto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $producto_id, $producto_tipo_id, $cotizacion_id, $cotizacion_RUC_cliente, $cotizacion_persona_cliente_id, $cotizacion_RUC_vendedor, $cotizacion_persona_vendedor_id)
    {
        if (($model = CotizacionProducto::findOne(['id' => $id, 'producto_id' => $producto_id, 'producto_tipo_id' => $producto_tipo_id, 'cotizacion_id' => $cotizacion_id, 'cotizacion_RUC_cliente' => $cotizacion_RUC_cliente, 'cotizacion_persona_cliente_id' => $cotizacion_persona_cliente_id, 'cotizacion_RUC_vendedor' => $cotizacion_RUC_vendedor, 'cotizacion_persona_vendedor_id' => $cotizacion_persona_vendedor_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
