<?php

namespace app\controllers;

use Yii;
use app\models\Oferta;
use app\models\OfertaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OfertaController implements the CRUD actions for Oferta model.
 */
class OfertaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Oferta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OfertaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Oferta model.
     * @param integer $id
     * @param integer $paquetes_id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $paquetes_id, $producto_id, $producto_tipo_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $paquetes_id, $producto_id, $producto_tipo_id),
        ]);
    }

    /**
     * Creates a new Oferta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Oferta();
        $modelsProducto = [new Producto];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'paquetes_id' => $model->paquetes_id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id]);
        }else{
            return $this->render('create', [
                'model' => $model,
                'modelsProducto' => (empty($modelsProducto)) ? [new Producto] : $modelsProducto
            ]);
        }
    }

    /**
     * Updates an existing Oferta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $paquetes_id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $paquetes_id, $producto_id, $producto_tipo_id)
    {
        $model = $this->findModel($id, $paquetes_id, $producto_id, $producto_tipo_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'paquetes_id' => $model->paquetes_id, 'producto_id' => $model->producto_id, 'producto_tipo_id' => $model->producto_tipo_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Oferta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $paquetes_id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $paquetes_id, $producto_id, $producto_tipo_id)
    {
        $this->findModel($id, $paquetes_id, $producto_id, $producto_tipo_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Oferta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $paquetes_id
     * @param integer $producto_id
     * @param integer $producto_tipo_id
     * @return Oferta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $paquetes_id, $producto_id, $producto_tipo_id)
    {
        if (($model = Oferta::findOne(['id' => $id, 'paquetes_id' => $paquetes_id, 'producto_id' => $producto_id, 'producto_tipo_id' => $producto_tipo_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
