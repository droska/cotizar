<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CotizacionProducto;

/**
 * CotizacionProductoSearch represents the model behind the search form of `app\models\CotizacionProducto`.
 */
class CotizacionProductoSearch extends CotizacionProducto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cantidad', 'producto_id', 'producto_tipo_id', 'cotizacion_id', 'cotizacion_RUC_cliente', 'cotizacion_persona_cliente_id', 'cotizacion_RUC_vendedor', 'cotizacion_persona_vendedor_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CotizacionProducto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cantidad' => $this->cantidad,
            'producto_id' => $this->producto_id,
            'producto_tipo_id' => $this->producto_tipo_id,
            'cotizacion_id' => $this->cotizacion_id,
            'cotizacion_RUC_cliente' => $this->cotizacion_RUC_cliente,
            'cotizacion_persona_cliente_id' => $this->cotizacion_persona_cliente_id,
            'cotizacion_RUC_vendedor' => $this->cotizacion_RUC_vendedor,
            'cotizacion_persona_vendedor_id' => $this->cotizacion_persona_vendedor_id,
        ]);

        return $dataProvider;
    }
}
