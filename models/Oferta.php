<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oferta".
 *
 * @property int $id
 * @property int $cantidad_productos
 * @property double $descuento
 * @property double $monto
 * @property int $paquetes_id
 * @property int $producto_id
 *
 * @property Paquetes $paquetes
 * @property Producto $producto
 */
class Oferta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oferta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cantidad_productos', 'descuento', 'monto', 'paquetes_id', 'producto_id'], 'required'],
            [['cantidad_productos', 'paquetes_id', 'producto_id'], 'integer'],
            [['descuento', 'monto'], 'number'],
            [['paquetes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquetes_id' => 'id']],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad_productos' => 'Cantidad Productos',
            'descuento' => 'Descuento',
            'monto' => 'Monto',
            'paquetes_id' => 'Paquetes ID',
            'producto_id' => 'Producto ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasOne(Paquetes::className(), ['id' => 'paquetes_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }
}
