<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion_producto".
 *
 * @property int $id
 * @property int $cantidad
 * @property int $producto_id
 * @property int $cotizacion_id
 *
 * @property Cotizacion $cotizacion
 * @property Producto $producto
 */
class CotizacionProducto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cotizacion_producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cantidad', 'producto_id', 'cotizacion_id'], 'required'],
            [['cantidad', 'producto_id', 'cotizacion_id'], 'integer'],
            [['cotizacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cotizacion::className(), 'targetAttribute' => ['cotizacion_id' => 'id']],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad' => 'Cantidad',
            'producto_id' => 'Producto ID',
            'cotizacion_id' => 'Cotizacion ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacion()
    {
        return $this->hasOne(Cotizacion::className(), ['id' => 'cotizacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }
}
