<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DetalleCotizacion;

/**
 * DetalleCotizacionSearch represents the model behind the search form of `app\models\DetalleCotizacion`.
 */
class DetalleCotizacionSearch extends DetalleCotizacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cotizacion_id', 'cotizacion_RUC_cliente', 'cotizacion_persona_rol_id', 'cotizacion_RUC_vendedor', 'cotizacion_persona_vendedor', 'paquetes_id'], 'integer'],
            [['subtotal', 'impuesto', 'total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalleCotizacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subtotal' => $this->subtotal,
            'impuesto' => $this->impuesto,
            'total' => $this->total,
            'cotizacion_id' => $this->cotizacion_id,
            'cotizacion_RUC_cliente' => $this->cotizacion_RUC_cliente,
            'cotizacion_persona_rol_id' => $this->cotizacion_persona_rol_id,
            'cotizacion_RUC_vendedor' => $this->cotizacion_RUC_vendedor,
            'cotizacion_persona_vendedor' => $this->cotizacion_persona_vendedor,
            'paquetes_id' => $this->paquetes_id,
        ]);

        return $dataProvider;
    }
}
