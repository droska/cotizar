<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property DetalleCotizacion[] $detalleCotizacions
 * @property Oferta[] $ofertas
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleCotizacions()
    {
        return $this->hasMany(DetalleCotizacion::className(), ['paquetes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfertas()
    {
        return $this->hasMany(Oferta::className(), ['paquetes_id' => 'id']);
    }
}
