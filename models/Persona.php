<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property string $RUC
 * @property string $nombre
 * @property int $rol_id
 *
 * @property Cotizacion[] $cotizacions
 * @property Cotizacion[] $cotizacions0
 * @property Rol $rol
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RUC', 'nombre', 'rol_id'], 'required'],
            [['RUC', 'rol_id'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['rol_id'], 'unique'],
            [['RUC', 'rol_id'], 'unique', 'targetAttribute' => ['RUC', 'rol_id']],
            [['rol_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['rol_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RUC' => 'Ruc',
            'nombre' => 'Nombre',
            'rol_id' => 'Rol ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacions()
    {
        return $this->hasMany(Cotizacion::className(), ['RUC_cliente' => 'RUC', 'cliente_id' => 'rol_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacions0()
    {
        return $this->hasMany(Cotizacion::className(), ['RUC_vendedor' => 'RUC', 'vendedor_id' => 'rol_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRol()
    {
        return $this->hasOne(Rol::className(), ['id' => 'rol_id']);
    }
}
