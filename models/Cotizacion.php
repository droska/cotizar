<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cotizacion".
 *
 * @property int $id
 * @property string $RUC_cliente
 * @property string $RUC_vendedor
 *
 * @property Persona $rUCCliente
 * @property Persona $rUCVendedor
 * @property CotizacionProducto[] $cotizacionProductos
 * @property DetalleCotizacion[] $detalleCotizacions
 */
class Cotizacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cotizacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RUC_cliente', 'RUC_vendedor'], 'required'],
            [['RUC_cliente', 'RUC_vendedor'], 'integer'],
            [['RUC_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['RUC_cliente' => 'RUC']],
            [['RUC_vendedor'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['RUC_vendedor' => 'RUC']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'RUC_cliente' => 'Ruc Cliente',
            'RUC_vendedor' => 'Ruc Vendedor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRUCCliente()
    {
        return $this->hasOne(Persona::className(), ['RUC' => 'RUC_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRUCVendedor()
    {
        return $this->hasOne(Persona::className(), ['RUC' => 'RUC_vendedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionProductos()
    {
        return $this->hasMany(CotizacionProducto::className(), ['cotizacion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleCotizacions()
    {
        return $this->hasMany(DetalleCotizacion::className(), ['cotizacion_id' => 'id']);
    }
}
