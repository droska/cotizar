<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $id
 * @property string $nombre
 * @property double $monto
 * @property int $tipo_id
 *
 * @property CotizacionProducto[] $cotizacionProductos
 * @property Oferta[] $ofertas
 * @property Tipo $tipo
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'monto', 'tipo_id'], 'required'],
            [['monto'], 'number'],
            [['tipo_id'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'monto' => 'Monto',
            'tipo_id' => 'Tipo ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacionProductos()
    {
        return $this->hasMany(CotizacionProducto::className(), ['producto_id' => 'id', 'producto_tipo_id' => 'tipo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfertas()
    {
        return $this->hasMany(Oferta::className(), ['producto_id' => 'id', 'producto_tipo_id' => 'tipo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_id']);
    }
}
