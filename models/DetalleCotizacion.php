<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalle_cotizacion".
 *
 * @property int $id
 * @property double $subtotal
 * @property double $impuesto
 * @property double $total
 * @property int $cotizacion_id
 * @property int $paquetes_id
 *
 * @property Cotizacion $cotizacion
 * @property Paquetes $paquetes
 */
class DetalleCotizacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detalle_cotizacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subtotal', 'impuesto', 'total', 'cotizacion_id', 'paquetes_id'], 'required'],
            [['subtotal', 'impuesto', 'total'], 'number'],
            [['cotizacion_id', 'paquetes_id'], 'integer'],
            [['cotizacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cotizacion::className(), 'targetAttribute' => ['cotizacion_id' => 'id']],
            [['paquetes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['paquetes_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subtotal' => 'Subtotal',
            'impuesto' => 'Impuesto',
            'total' => 'Total',
            'cotizacion_id' => 'Cotizacion ID',
            'paquetes_id' => 'Paquetes ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacion()
    {
        return $this->hasOne(Cotizacion::className(), ['id' => 'cotizacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasOne(Paquetes::className(), ['id' => 'paquetes_id']);
    }
}
